<?php get_header(); ?>

<div class="header__sock header-sock">
  <h1 class="header-sock__title"><?=pll__('say_main')?></h1>

          <div class="header-sock__middle header-sock__middle--links">

            <div class="wishes-links wishes-links--header">
              <ul class="wishes-links__list">
                <li class="wishes-links__item">
                  <h3 class="wishes-links__title"><?= pll__('sock_link')?></h3>
                  <div class="wishes-links__wrapper">
            				<a class="wishes-links__link" data-link="<?=$page_url?>"><?=$page_url?></a>
                    <a class="wishes-links__copy js-copy_link"><?=pll__('copy_paste')?></a>
                  </div>
                </li>
                <li class="wishes-links__item">
									<h3 class="wishes-links__title"><?=pll__('share_social')?></h3>
									<ul class="header-line__social social">
										<li class="social__item"><a onclick="Share.vkontakte('<?=addslashes($page_url)?>','<?=addslashes($page_title)?>','<?=addslashes($page_thumb_url)?>','<?=addslashes($page_description)?>')" class="social__link social__link--vk"></a></li>
										<li class="social__item"><a onclick="Share.facebook('<?=addslashes($page_url)?>','<?=addslashes($page_title)?>','<?=addslashes($page_thumb_url)?>','<?=addslashes($page_description)?>')" class="social__link social__link--fb"></a></li>
										<li class="social__item"><a onclick="Share.odnoklassniki('<?=addslashes($page_url)?>','<?=addslashes($page_title)?>','<?=addslashes($page_thumb_url)?>','<?=addslashes($page_description)?>')" class="social__link social__link--ok"></a></li>
									</ul>
								</li>
							</ul>
						</div>

          </div>

          <div class="header-sock__top">
            <a class="knit-your-wish" href="<?= home_url('/')?>"><?= pll__('make_yours')?></a>
          </div>

          <div class="header-sock__middle">
            <p class="header-sock__wishes"><?= $sock->post_content ?></p>
          </div>

          <div class="header-sock__end"></div>

          <div class="header-sock__bottom"></div>

          <span class="header-sock__label"><?= $sock->post_title ?></span>

          <div class="header-sock__copy">
            <div class="header-sock__top"></div>

            <div class="header-sock__middle">
              <p class="header-sock__wishes"><?= $sock->post_content ?></p>
            </div>

            <div class="header-sock__end"></div>

            <div class="header-sock__bottom"></div>
          </div>
        </div>

        <div class="header__line header-line">
          <a href="<?= home_url('/')?>" class="header-line__home header-line__link"><?=pll__('to_main')?></a>

          <a class="header-line__about header-line__link js-about_btn"><?=pll__('about_project')?></a>
        </div>
      </header>

      <main class="main">
        <div class="main__content">

          <a class="knit-your-wish" href="<?= home_url('/')?>"><?= pll__('make_yours')?></a>

        </div>
      </main>

      <div class="wishes-links">
        <ul class="wishes-links__list">
          <li class="wishes-links__item">
            <h3 class="wishes-links__title"><?= pll__('sock_link')?></h3>
            <a class="wishes-links__link" data-link="<?=$page_url?>"><?=$page_url?></a>
            <a class="wishes-links__copy js-copy_link"><?=pll__('copy_paste')?></a>
          </li>
          <li class="wishes-links__item">
            <h3 class="wishes-links__title"><?=pll__('share_social')?></h3>
						<ul class="header-line__social social">
							<li class="social__item"><a onclick="Share.vkontakte('<?=addslashes($page_url)?>','<?=addslashes($page_title)?>','<?=addslashes($page_thumb_url)?>','<?=addslashes($page_description)?>')" class="social__link social__link--vk"></a></li>
							<li class="social__item"><a onclick="Share.facebook('<?=addslashes($page_url)?>','<?=addslashes($page_title)?>','<?=addslashes($page_thumb_url)?>','<?=addslashes($page_description)?>')" class="social__link social__link--fb"></a></li>
							<li class="social__item"><a onclick="Share.odnoklassniki('<?=addslashes($page_url)?>','<?=addslashes($page_title)?>','<?=addslashes($page_thumb_url)?>','<?=addslashes($page_description)?>')" class="social__link social__link--ok"></a></li>
						</ul>
					</li>
        </ul>
      </div>

<?php get_footer(); ?>
