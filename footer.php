			<footer class="footer">
				<p class="footer__copy copy">© 2017 <?=pll__('copy')?></p>
				<p class="footer__made-by made-by">
					Made with sense by <a class="made-by__link" href="http://bitsens.com" target="_blank">Bitsens</a
				</p>
			</footer>
		</div>
		<?php
		$query = new WP_Query(
			array(
				'post_type' => 'page',
				'meta_key' => '_wp_page_template',
				'meta_value' => 'about.php'
			)
		);

		$query->the_post();
		?>
  <section class="about js-about_container">
    <a class="about__close js-about_close"></a>
    <div class="about__wrapper js-about_wrapper">
      <h2 class="about__title"><?php the_title()?></h2>

      <p class="about__description"><?php the_content()?></p>

      <ul class="about__terms terms">
        <li class="terms__item">
          <h3 class="terms__title"><?php the_field('about_info_lottery_title')?></h3>
          <p class="terms__description"><?php the_field('about_info_lottery_paragraph')?></p>
        </li>
        <li class="terms__item">
					<?php the_post_thumbnail( 'full', array('class'=>'terms_image', 'width' => '334', 'height' => '373') ); ?>
        </li>
      </ul>

      <ul class="about__shops shops">
				<?php
				$events_args = array(
						'post_type' => array( 'shoping_event' ),
						'posts_per_page' => -1,
						'meta_key' => 'shoping_event_info_shop_name',
						'orderby'   => 'meta_value',
						'order' => 'ASC'
						);
				$events = new WP_Query( $events_args );

				$cities = array();


				while ( $events->have_posts() ) {
					$events->the_post();
					$city = get_field('shoping_event_info_city');
					if (!isset($cities[$city])) {
						$cities[$city] = array();
					}
					$event = array(
						"name" => get_field('shoping_event_info_shop_name'),
						"address" => get_field('shoping_event_info_address'),
						"dates" => get_field('shoping_event_info_dates')
					);

					array_push($cities[$city], $event);
				}

				foreach ($cities as $key => $events) {
					?>
					<ul class="about__shops shops"> 
					<li class="shops__item">
						<div class="shops__left">
							<h4 class="shops__city"><?=$key?></h4>
						</div>
						<div class="shops__right">
							<p class="shops__month"><?=pll__('december')?></p>
						</div>
					</li>
				<?php

				foreach ($events as $event) {
					?>
					<li class="shops__item">
						<div class="shops__left">
							<h5 class="shops__name"><?= $event["name"]?></h5>
							<p class="shops__address"><?= $event["address"]?></p>
						</div>
						<div class="shops__right">
							<p class="shops__dates"><?= $event["dates"]?></p>
						</div>
					</li>
					<?php
				}
				 ?>
			 </ul>
				 <?php
			}
				?>
			</ul>
		</div>
	</section>

		<?php wp_footer(); ?>


	</body>
</html>
