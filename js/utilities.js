// Adds retina class
document.documentElement.className += ((window.devicePixelRatio >= 2) ? ' retina' : '');
// Adds hover class
document.documentElement.className += (("ontouchstart" in document.documentElement) && ((/iphone|ipod|ipad|android|blackberry|fennec/).test(navigator.userAgent.toLowerCase())) ? '' : ' hover');
// Adds "safari" class
document.documentElement.className += ((navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) ? ' safari' : '');
// Adds "android" class
document.documentElement.className += ((navigator.userAgent.indexOf('Android') != -1) ? ' android' : '');
// Adds "ie" class
document.documentElement.className += ((/msie/.test(navigator.userAgent.toLowerCase())) || (/trident/.test(navigator.userAgent.toLowerCase())) || (/edge/.test(navigator.userAgent.toLowerCase()))) ? ' ie' : '';
// Adds "ios" class
document.documentElement.className += (((/iphone|ipod|ipad/).test(navigator.userAgent.toLowerCase())) ? ' ios' : '');
