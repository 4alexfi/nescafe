// **
// Add More News Module
// **
var moreNews = (function ( $ ) {
  'use sctict';

  var $btn = $(".js-more_btn"),
      $container = $(".js-main_content"),
      pageNumber = 1;

  var _bindUIActions = function() {
    $btn.on("click", function(){
      if (!$btn.hasClass('is-loading')) {
        _getNewContent();
      }
    });
  };

  var _getNewContent = function() {
    pageNumber++;

    $.ajax({
      type: 'POST',
      dataType: 'html',
      url: ajax_posts.ajaxurl,
      data: {
        'action': 'load_more_socks',
        'pageNumber': pageNumber
      },
      beforeSend : function () {
        $btn.addClass("is-loading");
      },
      success: function (content) {
        $btn.removeClass("is-loading");
        if ((content === '') || (content === "0")) {
          $btn.hide();
        } else {
	_insertContent(content);
	var socksCount = $(content).filter('.main__sock').length;
	if (socksCount < 5) {
	$btn.hide();
	}
	}
      }
    });

    event.preventDefault();
    return false;
  }

  var _insertContent = function(newContent) {
    var $content = $(newContent);
    $content.find($(".sock")).addClass("is-invisible");
    $container.append($content);
    setTimeout(function(){
      _setZIndexes();
      $(".sock").removeClass("is-invisible");
    }, 50);
  }

  var _setZIndexes = function() {
    var $sock = $container.find(".sock");
    var socksNumber = $sock.length;

    for(var i = 0; i < socksNumber; i++) {
      $sock.eq(i).css("z-index", (socksNumber - i));
    }
    
    var attr = $(".main__content").attr('style');
    if (typeof attr !== typeof undefined && attr !== false) {
      $(".main__content").removeAttr("style");
    }

    checkPattenHeigh.checkSocks();

    $btn.removeClass("is-loading");
  }

  ready = function() {
    _bindUIActions();
    _setZIndexes();
  };

  return {
    ready: ready
  };

})( jQuery );


// **
// Show/Hide About Popup
// **
var aboutProject = (function ( $ ) {
  'use sctict';

  var $aboutBtn = $(".js-about_btn"),
      $aboutClose = $(".js-about_close"),
      $aboutContainer = $(".js-about_container"),
      $aboutWrapper = $(".js-about_wrapper"),
      startPos;

  var _bindUIActions = function() {
    $aboutBtn.on("click", function(){
      window.location.hash = "about";
      _goToTop();
      _openpopUp();

      return false;
    });

    $aboutClose.on("click", function(){
      window.location.hash = "";
      _closepopUp();

      return false;
    });

    $(document).on('keydown', function(e) {
      if (e.which == 27 && $aboutContainer.hasClass("is-active")) {
        window.location.hash = "";
        _closepopUp();

        return false;
      }
    });
  }

  var _goToTop = function() {
    startPos = $(window).scrollTop();
    $(window).scrollTop(0);
  }

  var _goToStartPos = function() {
    $(window).scrollTop(startPos);
  }

  var _openpopUp = function() {
//    $aboutContainer.css("height", $(document).height());
    $aboutContainer.addClass("is-active");
    $("body").addClass("is-popup");
    setTimeout(function() {
      $aboutWrapper.addClass("is-active");
    }, 330);
  };

  var _closepopUp = function() {
    $aboutWrapper.removeClass("is-active");
    $("body").removeClass("is-popup");
    setTimeout(function() {
      $aboutContainer.removeClass("is-active");
    }, 330);
  };

  var _checkHash = function() {
    if(window.location.hash != "" && window.location.hash == "#about") {
      _openpopUp();
    }
  }

  ready = function() {
    _checkHash();
    _bindUIActions();
  };

  return {
    ready: ready
  };
})( jQuery );


// **
// Show/Hide About Popup
// **
var innerPageAnimation = (function ( $ ) {
  'use sctict';

  var $sockWishes = $(".header-sock__wishes"),
      $sockWishesCopy = $(".header-sock__copy .header-sock__wishes"),
      $mainContainer = $(".main__content"),
      $sockBottom = $(".header-sock__bottom"),
      wishesHeight,
      wishesMaxHeight,
      wishesBottomHeight,
      animationSpeed = 120;

  var _getWishesHeights = function() {
    wishesHeight = $sockWishesCopy.innerHeight();
    wishesMaxHeight = parseInt($sockWishes.css("max-height").replace("px", ""));
    wishesBottomHeight = $sockBottom.height();
    $sockBottom.css("max-height", 0);
  };

  var _animateSock = function() {
    var newMaxHeight = parseInt($sockWishes.css("max-height").replace("px", "")) + 9;
    var newContentHeight = parseInt($mainContainer.css("min-height").replace("px", "")) + 9;
    var stopAnim = false;
    var fromTop = $(document).scrollTop();

    if(newMaxHeight > wishesHeight) {
      wishesNewHeight = wishesHeight;
      stopAnim = true;
    }

    $("body").on("mousewheel wheel", function() {
      return false;
    });

    $sockWishes.css("max-height", newMaxHeight);
    $mainContainer.css("min-height", newContentHeight);
    $(window).scrollTop(fromTop + 9);

    if($("html").hasClass("safari")) {
//      animationSpeed = 40;
    }

    if(!stopAnim) {
      setTimeout(function(){
        window.requestAnimationFrame(_animateSock);
      }, animationSpeed);
    }else{
      $sockBottom.show();
      _showSockBottom();
    }
  };

  var _showSockBottom = function() {
    var newMaxHeight = parseInt($sockBottom.css("max-height").replace("px", "")) + 9;
    var newContentHeight = parseInt($mainContainer.css("min-height").replace("px", "")) + 9;
    var fromTop = $(document).scrollTop();
    var stopAnim = false;

    if(newMaxHeight > wishesBottomHeight) {
      wishesNewHeight = wishesBottomHeight;
      stopAnim = true;
    }

    $sockBottom.css("max-height", newMaxHeight);
    $mainContainer.css("min-height", newContentHeight);
    $(window).scrollTop(fromTop + 9);

    if(!stopAnim) {
      setTimeout(function(){
        window.requestAnimationFrame(_showSockBottom);
      }, animationSpeed);
    }else{
      _showSockTop();
    }
  };

  var _showSockTop = function() {
    var sockTopHeight = $(".header-sock__top").outerHeight() + $(".header-sock__middle--links").outerHeight();
    var newContentHeight = parseInt($mainContainer.css("min-height").replace("px", "")) + ((sockTopHeight - 130) / 2);
    var fromTop = $(document).scrollTop();

    $(".header-sock__top").show();
    $(".header-sock__middle--links").show();
    $mainContainer.css("min-height", newContentHeight);
    $(window).scrollTop(fromTop + sockTopHeight);

    var $sockPatternContainer = $(".header-sock__wishes"),
        sockPatternHeight = 9,
        $mainPatternConteiner = $(".main__content"),
        mainPatternHeight = 269;

    if($sockPatternContainer.outerHeight() % sockPatternHeight != 0) {
      $sockPatternContainer.css("height", Math.ceil( ($sockPatternContainer.outerHeight() / sockPatternHeight) ) * sockPatternHeight );
    }

    if($mainPatternConteiner.outerHeight() % mainPatternHeight != 0) {
      $mainPatternConteiner.css("height", Math.ceil( ($mainPatternConteiner.outerHeight() / mainPatternHeight) ) * mainPatternHeight );
    }

    setTimeout(function(){
      $("body").off("mousewheel wheel");
      santaActions.staticSanta();
      $(".header-sock").addClass("is-active");
      $(".wishes-links").addClass("is-active");
      $(".knit-your-wish").addClass("is-active");
    }, 500);
  };

  ready = function() {
    _getWishesHeights();
    setTimeout(function() {
      if($(document).scrollTop() > 250) {
        $(document).scrollTop(0);
      }
      santaActions.knit();
      _animateSock();
    }, 1500);
  };

  return {
    ready: ready
  };
})( jQuery );


// **
// Copy To Clipboard
// **
var copyToClipboard = (function ( $ ) {
  'use sctict';

  var $copyButton = $(".js-copy_link");
  var $copySource = $copyButton.prev("a");
  var message;

  var _createInput = function() {
    var $input = $('<input id="message" type="text" />');

    $("body").append($input);
    $input.css("opacity", 0);
  };

  var _addContentToInput = function() {
    message = document.getElementById("message");
    message.value = $copySource.data("link");
  };

  var _copyContentToClipboard = function() {
    message.select();
    document.execCommand('copy');
    $copyButton.addClass("is-active");
    setTimeout(function() {
      $copyButton.removeClass("is-active");
    }, 1500);
  };

  var _bindUIActions = function() {
    $copyButton.on("click", function(){
      _createInput();
      _addContentToInput();
      _copyContentToClipboard();

      return false;
    });
  };

  ready = function() {
    _bindUIActions();
  };

  return {
    ready: ready
  };
})( jQuery );


// **
// Counters
// **
var counterFunctions = (function ( $ ) {
  var $counter = $(".js-counter");
  var numberLength = 6;

  var _transformValue = function(i) {
    var $el = $counter.eq(i)
    var number = $el.data("number");
    number = _addZeros(number);
    var numberArray = _getDigitsArray(number);

    _putValuesToCounter(i, numberArray);
  };

  var _addZeros = function(number) {
    number = number.toString();
    return number.length < numberLength ? _addZeros("0" + number, numberLength) : number;
  };

  var _getDigitsArray = function(number) {
    var numArray = [];
    for(var i = 0, l = number.length; i < l; i++) {
      numArray.push(+number.charAt(i));
    }
    return numArray;
  };

  var _putValuesToCounter = function(i, numberArray) {
    var length = numberArray.length;
    while(length > -1) {
      $counter.eq(i).find("li").eq(length).find("p").html(numberArray[length]);
      length--;
    }
    $counter.eq(i).addClass("is-active");
  };

  var _startCounters = function() {
    for(var i = 0, l = $counter.length; i < l; i++) {
      _transformValue(i);
    }
  }

  ready = function() {
    _startCounters();
  };

  return {
    ready: ready
  };
})( jQuery );


// **
// Form Actions
// **
var formActions = (function ( $ ) {
  var $form = $(".js-form"),
      $formInput = $(".js-input"),
      $formError = $(".js-form_error"),
      counterStart = 300,
      $counter,
      nameCounterStart = 30,
      $nameCounter,
      maxLines = 7;

  var _checkValueOnBlur = function( $el ) {
    if( $el.val() != "" ) {
      $el.addClass("has-value");
    } else {
      $el.removeClass("has-value");
    }
  };

  var _checkOnFocus = function() {
    if($formError.hasClass("is-shown")) {
      $formError.removeClass("is-shown");
    }

  };

  var _textareaOnMoreLines = function( $el ) {
    setTimeout(function(){
      if(($el.val().match(/\n/g) || []).length < maxLines){
        $el.css("height", "auto");
        $el.css("height", $el[0].scrollHeight + "px");
      }

      if( $el.val() == "" ) {
        $el.removeAttr("style");
      }
    }, 50);
  };

  var _submitForm = function() {
    var errorFlag = false;

    for(var i = 0, l = $formInput.length; i < l; i++) {
      if($formInput.eq(i).val() == "" ) {
        errorFlag = true;
      }
    }

    if(errorFlag) {
      $formError.addClass("is-shown");
      return false;
    } else {
      $('input[type=text]').prop('disabled', true);
      return true;
    }
  };

  var _sendFormDataToServer = function() {
    console.log("Function to send info");
  };

  var _addCounter = function() {
    $counter = $('<span class="form__counter"></span>');
    $("textarea").attr("maxlength", counterStart);
    $("textarea").closest(".form__item").append( $counter );
    _showCounter( $counter, 0 );
  };

  var _showCounter = function( $counter, num ) {
    $counter.html( counterStart - num );
  };

  var _addNameCounter = function() {
    $nameCounter = $('<span class="form__counter is-name"></span>');
    $("input[type=text]").attr("maxlength", nameCounterStart);
    $("input[type=text]").closest(".form__item").append( $nameCounter );
    _showNameCounter( $nameCounter, 0 );
  };

  var _showNameCounter = function( $nameCounter, num ) {
    $nameCounter.html( nameCounterStart - num );
  };

  var _bindUIActions = function() {
    $formInput.on("blur", function(){
      _checkValueOnBlur( $(this) );
      santaActions.staticSanta();
    });

    $formInput.on("focus", function() {
      _checkOnFocus();

      santaActions.knit();
    });

    $('textarea').on('input', function () {
      _textareaOnMoreLines( $(this) );
      _showCounter( $counter, $(this).val().length )
    });
    $('input[type=text]').on('input', function () {
      _showNameCounter( $nameCounter, $(this).val().length )
    });

    $form.on("submit", function() {
      return _submitForm();
    });
  };

  ready = function() {
    _bindUIActions();
    _addCounter();
    _addNameCounter();
  };

  return {
    ready: ready
  };
})( jQuery );


// **
// Check patterns height
// **
var checkPattenHeigh = (function ( $ ) {
  'use sctict';

  var $sock,
      $sockPatternContainer,
      sockPatternHeight = 9,
      $mainPatternConteiner,
      mainPatternHeight = 269;
  
  var _setVars = function() {
    $sock = $(document).find(".sock"),
    $sockPatternContainer = $(document).find(".sock__active .sock__text"),
    $mainPatternConteiner = $(document).find(".main__content");
  };

  var _checkSocksFunction = function() {
    for( var i = 0, l = $sockPatternContainer.length; i < l; i++) {
      if($sock.eq(i).find($sockPatternContainer).outerHeight() % sockPatternHeight != 0) {
        $sock.eq(i).find($sockPatternContainer).css("height", Math.ceil( ($sock.eq(i).find($sockPatternContainer).outerHeight() / sockPatternHeight) ) * sockPatternHeight );
      }
    }
  };

  var _checkMainPatternFunction = function() {
    if($mainPatternConteiner.outerHeight() % mainPatternHeight != 0) {
      $mainPatternConteiner.css("height", Math.ceil( ($mainPatternConteiner.outerHeight() / mainPatternHeight) ) * mainPatternHeight );
    }
  };

  checkSocks = function() {
    _setVars();
    setTimeout(function(){
      _checkSocksFunction();
      _checkMainPatternFunction();
    }, 500);
  };

  return {
    checkSocks: checkSocks
  };
})( jQuery );


// **
// Santa Actions
// **
var santaActions = (function ( $ ) {
  'use sctict';

  var $santa = $(".js-santa"),
      defaultSanta,
      winkSanta,
      knittingSanta;

  var _getSantas = function() {
    defaultSanta = $santa.attr("src");
    winkSanta = $santa.data("wink");
    knittingSanta = $santa.data("gif");

    _preloadSantas( winkSanta, knittingSanta );
  };

  var _preloadSantas = function() {
    var images = [];
    for (i = 0; i < _preloadSantas.arguments.length; i++) {
      images[i] = new Image()
      images[i].src = _preloadSantas.arguments[i]
    }
  }

  var _changeSanta = function( santa ){
    $santa.attr("src", santa);
  };

  var wink = function() {
    if($("html").hasClass("hover")) {
      $santa.on("mouseenter", function(){
      _changeSanta( winkSanta );
    }).on("mouseleave", function(){
      var check = false;

      for(var i = 0, l = $(".js-input").length; i < l; i++) {
        if($(".js-input").eq(i).is(":focus")) {
          check = true;
        }
      }

      if(check) {
        knit();
      }else{
        staticSanta();
      }
    });
    }
  };

  var knit = function() {
    _changeSanta( knittingSanta );
  };

  var staticSanta = function() {
    _changeSanta( defaultSanta );
  };

  var ready = function() {
    _getSantas();
    wink();
  };


  return {
    ready: ready,
    wink: wink,
    knit: knit,
    staticSanta: staticSanta
  };
})( jQuery );


// **
// Hide/Show Made By
// **
var watermark = (function ( $ ) {
  'use strict';
  
  var startTime = 2,
      endTime = 6,
      currentTime;
  
  var _getCurrentTime = function() {
    currentTime = new Date().getHours();
    
    _watermarkAction();
  };
  
  var _watermarkAction = function() {
    if(currentTime < startTime || currentTime > endTime) {
      $(".made-by").css({
        "opacity": 0,
        "pointer-events": "none"
      });
    }
  };
  
  var ready = function() {
    _getCurrentTime();
  };


  return {
    ready: ready
  };
})( jQuery );

// **
// Start Functions on DOC.Ready
// **
$(function(){
  watermark.ready();
  moreNews.ready();
  aboutProject.ready();
  counterFunctions.ready();
  santaActions.ready();
  if($(".js-form").length) {
    formActions.ready();
  }

  if($("body").hasClass("inner")) {
    innerPageAnimation.ready();
    copyToClipboard.ready();
  }else{
    checkPattenHeigh.checkSocks();
  }
});
