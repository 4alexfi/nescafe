$(function() {
  var pageNumber = 0;
  var $loadMoreButton = $('.js-load-more');
  var $newsLoader = $('.js-news-loader');

  $loadMoreButton.click(function(event) {
    pageNumber++;
    $loadMoreButton.hide();
    $newsLoader.show();
    $.ajax({
      type: 'POST',
      dataType: 'html',
      url: ajax_posts.ajaxurl,
      data: {
        'action': 'load_more_news',
        'pageNumber': pageNumber
      },
      beforeSend : function () {
        // Add spinner or something
      },
      success: function (data) {
        $loadMoreButton.show();
        $newsLoader.hide();
        $('.js-news-container').append(data);
        if (data === "") {
          $loadMoreButton.hide();
        }
      }
    });

    event.preventDefault();
    return false;
  })
})
