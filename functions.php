<?php
/*
*  Author: Todd Motto | @toddmotto
*  URL: nescafe.com | @nescafe
*  Custom functions, support, custom post types and more.
*/

/*------------------------------------*\
External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
Theme Support
\*------------------------------------*/

if (!isset($content_width))
{
  $content_width = 900;
}

if (function_exists('add_theme_support'))
{
  // Add Menu Support
  add_theme_support('menus');

  // Add Thumbnail Theme Support
  add_theme_support('post-thumbnails');
  add_image_size('large', 700, '', true); // Large Thumbnail
  add_image_size('medium', 250, '', true); // Medium Thumbnail
  add_image_size('small', 120, '', true); // Small Thumbnail
  add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

  // Add Support for Custom Backgrounds - Uncomment below if you're going to use
  /*add_theme_support('custom-background', array(
  'default-color' => 'FFF',
  'default-image' => get_template_directory_uri() . '/img/bg.jpg'
));*/

// Add Support for Custom Header - Uncomment below if you're going to use
/*add_theme_support('custom-header', array(
'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
'header-text'			=> false,
'default-text-color'		=> '000',
'width'				=> 1000,
'height'			=> 198,
'random-default'		=> false,
'wp-head-callback'		=> $wphead_cb,
'admin-head-callback'		=> $adminhead_cb,
'admin-preview-callback'	=> $adminpreview_cb
));*/

// Enables post and comment RSS feed links to head
add_theme_support('automatic-feed-links');

// Localisation Support
load_theme_textdomain('nescafe', get_template_directory() . '/languages');
}

/*------------------------------------*\
Functions
\*------------------------------------*/

// HTML5 Blank navigation
function nescafe_nav()
{
  wp_nav_menu(
    array(
      'theme_location'  => 'header-menu',
      'menu'            => '',
      'container'       => 'div',
      'container_class' => 'menu-{menu slug}-container',
      'container_id'    => '',
      'menu_class'      => 'menu',
      'menu_id'         => '',
      'echo'            => true,
      'fallback_cb'     => 'wp_page_menu',
      'before'          => '',
      'after'           => '',
      'link_before'     => '',
      'link_after'      => '',
      'items_wrap'      => '<ul>%3$s</ul>',
      'depth'           => 0,
      'walker'          => ''
    )
  );
}

// Load HTML5 Blank scripts (header.php)
function nescafe_scripts()
{
  if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

    wp_register_script('utilities', get_template_directory_uri() . '/js/utilities.js'); // Conditional script(s)
    wp_enqueue_script('utilities'); // Enqueue it!

    wp_register_script('share', get_template_directory_uri() . '/js/share.js'); // Conditional script(s)
    wp_enqueue_script('share'); // Enqueue it!

    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', get_template_directory_uri() . '/js/vendor/jquery.3.1.0.min.js', array(), '3.1.0', true );

    wp_register_script('main', get_template_directory_uri() . '/js/main.js', array('jquery'), '1.0.0', true); // Conditional script(s)
    wp_enqueue_script('main'); // Enqueue it!

    wp_localize_script( 'main', 'ajax_posts', array(
      'ajaxurl' => admin_url( 'admin-ajax.php' )
    ));
  }
}

// Load HTML5 Blank styles
function nescafe_styles()
{
  wp_register_style('nescafe', get_template_directory_uri() . '/main.css', array(), '1.0', 'all');
  wp_enqueue_style('nescafe'); // Enqueue it!
}

// Register HTML5 Blank Navigation
// function register_html5_menu()
// {
//   register_nav_menus(array( // Using array to specify more menus if needed
//     'header-menu' => __('Header Menu', 'nescafe'), // Main Navigation
//     'sidebar-menu' => __('Sidebar Menu', 'nescafe'), // Sidebar Navigation
//     'extra-menu' => __('Extra Menu', 'nescafe') // Extra Navigation if needed (duplicate as many as you need!)
//   ));
// }

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
  $args['container'] = false;
  return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
  return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
  return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
  global $post;
  if (is_home()) {
    $key = array_search('blog', $classes);
    if ($key > -1) {
      unset($classes[$key]);
    }
  } elseif (is_page()) {
    $classes[] = sanitize_html_class($post->post_name);
  } elseif (is_singular()) {
    $classes[] = sanitize_html_class($post->post_name);
    array_push($classes, "inner");
  }

  array_push($classes, "body");

  return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
  // Define Sidebar Widget Area 1
  register_sidebar(array(
    'name' => __('Widget Area 1', 'nescafe'),
    'description' => __('Description for this widget-area...', 'nescafe'),
    'id' => 'widget-area-1',
    'before_widget' => '<div id="%1$s" class="%2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
  ));

  // Define Sidebar Widget Area 2
  register_sidebar(array(
    'name' => __('Widget Area 2', 'nescafe'),
    'description' => __('Description for this widget-area...', 'nescafe'),
    'id' => 'widget-area-2',
    'before_widget' => '<div id="%1$s" class="%2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
  ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
  global $wp_widget_factory;
  remove_action('wp_head', array(
    $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
    'recent_comments_style'
  ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
  global $wp_query;
  $big = 999999999;
  echo paginate_links(array(
    'base' => str_replace($big, '%#%', get_pagenum_link($big)),
    'format' => '?paged=%#%',
    'current' => max(1, get_query_var('paged')),
    'total' => $wp_query->max_num_pages
  ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
  return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
  return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
  global $post;
  if (function_exists($length_callback)) {
    add_filter('excerpt_length', $length_callback);
  }
  if (function_exists($more_callback)) {
    add_filter('excerpt_more', $more_callback);
  }
  $output = get_the_excerpt();
  $output = apply_filters('wptexturize', $output);
  $output = apply_filters('convert_chars', $output);
  $output = '<p>' . $output . '</p>';
  echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
  global $post;
  return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'nescafe') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
  return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
  return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
  $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
  return $html;
}

// Custom Gravatar in Settings > Discussion
function nescafegravatar ($avatar_defaults)
{
  $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
  $avatar_defaults[$myavatar] = "Custom Gravatar";
  return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
  if (!is_admin()) {
    if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script('comment-reply');
    }
  }
}

// Custom Comments Callback
function nescafecomments($comment, $args, $depth)
{
  $GLOBALS['comment'] = $comment;
  extract($args, EXTR_SKIP);

  if ( 'div' == $args['style'] ) {
    $tag = 'div';
    $add_below = 'comment';
  } else {
    $tag = 'li';
    $add_below = 'div-comment';
  }
  ?>
  <!-- heads up: starting < for the html tag (li or div) in the next line: -->
  <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
  <?php if ( 'div' != $args['style'] ) : ?>
    <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
    <?php endif; ?>
    <div class="comment-author vcard">
      <?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
      <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
    </div>
    <?php if ($comment->comment_approved == '0') : ?>
      <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
      <br />
    <?php endif; ?>

    <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
      <?php
      printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
      ?>
    </div>

    <?php comment_text() ?>

    <div class="reply">
      <?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </div>
    <?php if ( 'div' != $args['style'] ) : ?>
    </div>
  <?php endif; ?>
  <?php }

  /*------------------------------------*\
  Actions + Filters + ShortCodes
  \*------------------------------------*/

  // Add Actions
  add_action('init', 'nescafe_scripts'); // Add Custom Scripts to wp_head
  add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
  add_action('wp_enqueue_scripts', 'nescafe_styles'); // Add Theme Stylesheet
  // add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
  // add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
  add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
  add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

  // Remove Actions
  remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
  remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
  remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
  remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
  remove_action('wp_head', 'index_rel_link'); // Index link
  remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
  remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
  remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
  remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
  remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
  remove_action('wp_head', 'rel_canonical');
  remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

  // Add Filters
  add_filter('avatar_defaults', 'nescafegravatar'); // Custom Gravatar in Settings > Discussion
  add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
  add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
  add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
  add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
  // add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
  // add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
  // add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
  add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
  add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
  add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
  add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
  add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
  add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
  add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
  add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

  // Remove Filters
  remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

  // Shortcodes
  add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
  add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

  // Shortcodes above would be nested like this -
  // [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

  /*------------------------------------*\
  Custom Post Types
  \*------------------------------------*/

  // Create 1 Custom Post type for a Demo, called HTML5-Blank
  function create_post_type_html5()
  {
    register_taxonomy_for_object_type('category', 'html5-blank'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'html5-blank');
    register_post_type('html5-blank', // Register Custom Post Type
    array(
      'labels' => array(
        'name' => __('HTML5 Blank Custom Post', 'nescafe'), // Rename these to suit
        'singular_name' => __('HTML5 Blank Custom Post', 'nescafe'),
        'add_new' => __('Add New', 'nescafe'),
        'add_new_item' => __('Add New HTML5 Blank Custom Post', 'nescafe'),
        'edit' => __('Edit', 'nescafe'),
        'edit_item' => __('Edit HTML5 Blank Custom Post', 'nescafe'),
        'new_item' => __('New HTML5 Blank Custom Post', 'nescafe'),
        'view' => __('View HTML5 Blank Custom Post', 'nescafe'),
        'view_item' => __('View HTML5 Blank Custom Post', 'nescafe'),
        'search_items' => __('Search HTML5 Blank Custom Post', 'nescafe'),
        'not_found' => __('No HTML5 Blank Custom Posts found', 'nescafe'),
        'not_found_in_trash' => __('No HTML5 Blank Custom Posts found in Trash', 'nescafe')
      ),
      'public' => true,
      'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
      'has_archive' => true,
      'supports' => array(
        'title',
        'editor',
        'excerpt',
        'thumbnail'
      ), // Go to Dashboard Custom HTML5 Blank post for supports
      'can_export' => true, // Allows export in Tools > Export
      'taxonomies' => array(
        'post_tag',
        'category'
      ) // Add Category and Post Tags support
    ));
  }

  /*------------------------------------*\
  ShortCode Functions
  \*------------------------------------*/

  // Shortcode Demo with Nested Capability
  function html5_shortcode_demo($atts, $content = null)
  {
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
  }

  // Shortcode Demo with simple <h2> tag
  function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
  {
    return '<h2>' . $content . '</h2>';
  }

add_action('wp_ajax_load_more_socks', 'load_more_socks');
add_action('wp_ajax_nopriv_load_more_socks', 'load_more_socks');

function load_more_socks(){

    $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 1;

    header("Content-Type: text/html");

    $args = array(
      'post_type' => 'post',
      'posts_per_page' => 5,
      'category_name' => 'Featured',
      'paged' => $page
    );

    $loop = new WP_Query($args);

    $out = '';

    if ($loop -> have_posts()) :  while ($loop -> have_posts()) : $loop -> the_post();
        $out .= '<div class="main__sock sock">
          <div class="sock__active">
            <div class="sock__top"></div>
            <div class="sock__body">
              <p class="sock__text">
								'.get_the_content().'
              </p>
            </div>
            <div class="sock__bottom"></div>
            <span class="sock__label">'.get_the_title('', '').'</span>
          </div>

          <div class="sock__copy">
            <div class="sock__top"></div>
            <div class="sock__body">
              <p class="sock__text">
								'.get_the_content().'
              </p>
            </div>
            <div class="sock__bottom"></div>
          </div>
        </div>';
    endwhile;
    endif;
    wp_reset_postdata();
    die($out);
}

remove_filter('the_content', 'wpautop');

add_action('init', 'register_strings');

function register_strings() {
  // Копировать
  pll_register_string( 'copy_paste', 'copy_paste');

  pll_register_string( 'header_top_left', 'header_top_left', 'header', true);
  //стран увидели<br />наши поздравления
  pll_register_string( 'header_top_right', 'header_top_right', 'header', true);
                  // Нет слов – нет носков!
  pll_register_string( 'no_words_no_socks', 'no_words_no_socks', 'header', true);
  // О проекте
  pll_register_string( 'about_project', 'about_project', 'strings');
  pll_register_string( 'minsk', 'minsk');
  pll_register_string( 'december', 'december');
  // Больше<br />поздравлений
  pll_register_string( 'more_socks', 'more_socks', 'strings', true);
  // Связать Главные Тёплые Слова
  pll_register_string( 'create_sock', 'create_sock');
  // Ваша подпись
  pll_register_string( 'your_name', 'your_name');
  // Напишите ваши тёплые слова и я их свяжу
  pll_register_string( 'write_something', 'write_something');
  // Скажите<br />главное с
  pll_register_string( 'say_main', 'say_main');
  // Или поделитесь в соц. сетях
  pll_register_string( 'share_social', 'share_social');
  //На главную
  pll_register_string( 'to_main', 'to_main');
// Связать своё поздравление
pll_register_string( 'make_yours', 'make_yours');
// ССЫЛКА ЭТОГО НОСКА
pll_register_string( 'sock_link', 'sock_link');
  // Nestlé.
  pll_register_string( 'copy', 'copy');
}
function remove_menus(){

  remove_menu_page( 'index.php' );                  //Dashboard
  // remove_menu_page( 'edit.php' );                   //Posts
  // remove_menu_page( 'upload.php' );                 //Media
  // remove_menu_page( 'edit.php?post_type=page' );    //Pages
  remove_menu_page( 'edit-comments.php' );          //Comments
  remove_menu_page( 'themes.php' );                 //Appearance
  // remove_menu_page( 'plugins.php' );                //Plugins
  // remove_menu_page( 'users.php' );                  //Users
  // remove_menu_page( 'tools.php' );                  //Tools
  // remove_menu_page( 'options-general.php' );        //Settings

  add_menu_page(
    'Translations',
      'Translations',
      'manage_options',
      'options-general.php?page=mlang&tab=strings',
      '',
      '',
      6
  );
}
add_action( 'admin_menu', 'remove_menus' );

class Options {
	private $options_options;

	public function __construct() {
		add_action( 'admin_menu', array( $this, 'options_add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'options_page_init' ) );
	}

	public function options_add_plugin_page() {
		add_menu_page(
			'Options', // page_title
			'Options', // menu_title
			'manage_options', // capability
			'options', // menu_slug
			array( $this, 'options_create_admin_page' ), // function
			'dashicons-admin-generic', // icon_url
			3 // position
		);
	}

	public function options_create_admin_page() {
		$this->options_options = get_option( 'options_option_name' ); ?>

		<div class="wrap">
			<h2>Options</h2>
			<p></p>
			<?php settings_errors(); ?>

			<form method="post" action="options.php">
				<?php
					settings_fields( 'options_option_group' );
					do_settings_sections( 'options-admin' );
					submit_button();
				?>
			</form>
		</div>
	<?php }

	public function options_page_init() {

		register_setting(
			'options_option_group', // option_group
			'options_option_name', // option_name
			array( $this, 'options_sanitize' ) // sanitize_callback
		);

		add_settings_section(
			'options_setting_section', // id
			'Settings', // title
			array( $this, 'options_section_info' ), // callback
			'options-admin' // page
		);

		add_settings_field(
			'country_counter', // id
			'Country Counter', // title
			array( $this, 'country_counter_callback' ), // callback
			'options-admin', // page
			'options_setting_section' // section
		);

		add_settings_field(
			'share_thumb', // id
			'Share Thumbnail URL', // title
			array( $this, 'share_thumb_callback' ), // callback
			'options-admin', // page
			'options_setting_section' // section
		);
	}

	public function options_sanitize($input) {
		$sanitary_values = array();

		if ( isset( $input['country_counter'] ) ) {
			$sanitary_values['country_counter'] = (int)sanitize_text_field( $input['country_counter'] );
		}

		if ( isset( $input['share_thumb'] ) ) {
			$sanitary_values['share_thumb'] = $input['share_thumb'];
		}

		return $sanitary_values;
	}

	public function options_section_info() {

	}

	public function country_counter_callback() {
		printf(
			'<input class="regular-text" type="number" name="options_option_name[country_counter]" id="country_counter" value="%s">',
			isset( $this->options_options['country_counter'] ) ? esc_attr( $this->options_options['country_counter']) : ''
		);
	}

	public function share_thumb_callback() {
		printf(
			'<input class="regular-text" type="text" name="options_option_name[share_thumb]" id="share_thumb" value="%s">',
			isset( $this->options_options['share_thumb'] ) ? esc_attr( $this->options_options['share_thumb']) : ''
		);
	}

}
if ( is_admin() )
	$options = new Options();

  function info_get_meta( $value ) {
	global $post;

	$field = get_post_meta( $post->ID, $value, true );
	if ( ! empty( $field ) ) {
		return is_array( $field ) ? stripslashes_deep( $field ) : stripslashes( wp_kses_decode_entities( $field ) );
	} else {
		return false;
	}
}


// Register Custom Post Type
function shop_post_type() {

	$labels = array(
		'name'                  => _x( 'Shops', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Shop', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Shoping events', 'text_domain' ),
		'name_admin_bar'        => __( 'Post Type', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Shop', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'supports' => array(
      'title',
    ), // Go to Dashboard Custom HTML5 Blank post for supports
	);
	register_post_type( 'shoping_event', $args );

}
add_action( 'init', 'shop_post_type', 0 );
function shoping_event_info_get_meta( $value ) {
	global $post;

	$field = get_post_meta( $post->ID, $value, true );
	if ( ! empty( $field ) ) {
		return is_array( $field ) ? stripslashes_deep( $field ) : stripslashes( wp_kses_decode_entities( $field ) );
	} else {
		return false;
	}
}

function shoping_event_info_add_meta_box() {
	add_meta_box(
		'shoping_event_info-shoping-event-info',
		__( 'Shoping Event Info', 'shoping_event_info' ),
		'shoping_event_info_html',
		'shoping_event',
		'normal',
		'high'
	);
}
add_action( 'add_meta_boxes', 'shoping_event_info_add_meta_box' );

function shoping_event_info_html( $post) {
	wp_nonce_field( '_shoping_event_info_nonce', 'shoping_event_info_nonce' ); ?>

	<p>
		<label for="shoping_event_info_city"><?php _e( 'City', 'shoping_event_info' ); ?></label><br>
		<input type="text" name="shoping_event_info_city" id="shoping_event_info_city" value="<?php echo shoping_event_info_get_meta( 'shoping_event_info_city' ); ?>">
	</p>	<p>
		<label for="shoping_event_info_shop_name"><?php _e( 'Shop Name', 'shoping_event_info' ); ?></label><br>
		<input type="text" name="shoping_event_info_shop_name" id="shoping_event_info_shop_name" value="<?php echo shoping_event_info_get_meta( 'shoping_event_info_shop_name' ); ?>">
	</p>	<p>
		<label for="shoping_event_info_address"><?php _e( 'Address', 'shoping_event_info' ); ?></label><br>
		<input type="text" name="shoping_event_info_address" id="shoping_event_info_address" value="<?php echo shoping_event_info_get_meta( 'shoping_event_info_address' ); ?>">
	</p>	<p>
		<label for="shoping_event_info_dates"><?php _e( 'Dates', 'shoping_event_info' ); ?></label><br>
		<input type="text" name="shoping_event_info_dates" id="shoping_event_info_dates" value="<?php echo shoping_event_info_get_meta( 'shoping_event_info_dates' ); ?>">
	</p><?php
}

function shoping_event_info_save( $post_id ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
	if ( ! isset( $_POST['shoping_event_info_nonce'] ) || ! wp_verify_nonce( $_POST['shoping_event_info_nonce'], '_shoping_event_info_nonce' ) ) return;
	if ( ! current_user_can( 'edit_post', $post_id ) ) return;

	if ( isset( $_POST['shoping_event_info_city'] ) )
		update_post_meta( $post_id, 'shoping_event_info_city', esc_attr( $_POST['shoping_event_info_city'] ) );
	if ( isset( $_POST['shoping_event_info_shop_name'] ) )
		update_post_meta( $post_id, 'shoping_event_info_shop_name', esc_attr( $_POST['shoping_event_info_shop_name'] ) );
	if ( isset( $_POST['shoping_event_info_address'] ) )
		update_post_meta( $post_id, 'shoping_event_info_address', esc_attr( $_POST['shoping_event_info_address'] ) );
	if ( isset( $_POST['shoping_event_info_dates'] ) )
		update_post_meta( $post_id, 'shoping_event_info_dates', esc_attr( $_POST['shoping_event_info_dates'] ) );
}
add_action( 'save_post', 'shoping_event_info_save' );

function about_info_get_meta( $value ) {
	global $post;

	$field = get_post_meta( $post->ID, $value, true );
	if ( ! empty( $field ) ) {
		return is_array( $field ) ? stripslashes_deep( $field ) : stripslashes( wp_kses_decode_entities( $field ) );
	} else {
		return false;
	}
}

function about_info_add_meta_box() {
  global $post;
  if ( 'about.php' == get_post_meta( $post->ID, '_wp_page_template', true ) ) {
    add_meta_box(
      'about_info-about-info',
      __( 'About Info', 'about_info' ),
      'about_info_html',
      'page',
      'normal',
      'high'
    );
  }
}
add_action( 'add_meta_boxes', 'about_info_add_meta_box' );

function about_info_html( $post) {
	wp_nonce_field( '_about_info_nonce', 'about_info_nonce' ); ?>

	<p>
		<label for="about_info_lottery_title"><?php _e( 'Lottery title', 'about_info' ); ?></label><br>
		<input type="text" name="about_info_lottery_title" id="about_info_lottery_title" value="<?php echo about_info_get_meta( 'about_info_lottery_title' ); ?>">
	</p>	<p>
		<label for="about_info_lottery_paragraph"><?php _e( 'Lottery paragraph', 'about_info' ); ?></label><br>
		<textarea name="about_info_lottery_paragraph" id="about_info_lottery_paragraph" ><?php echo about_info_get_meta( 'about_info_lottery_paragraph' ); ?></textarea>

	</p><?php
}

function about_info_save( $post_id ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
	if ( ! isset( $_POST['about_info_nonce'] ) || ! wp_verify_nonce( $_POST['about_info_nonce'], '_about_info_nonce' ) ) return;
	if ( ! current_user_can( 'edit_post', $post_id ) ) return;

	if ( isset( $_POST['about_info_lottery_title'] ) )
		update_post_meta( $post_id, 'about_info_lottery_title', esc_attr( $_POST['about_info_lottery_title'] ) );
	if ( isset( $_POST['about_info_lottery_paragraph'] ) )
		update_post_meta( $post_id, 'about_info_lottery_paragraph', esc_attr( $_POST['about_info_lottery_paragraph'] ) );
}
add_action( 'save_post', 'about_info_save' );

add_filter( 'views_edit-post', function( $views )
{
    $remove_views = [ 'mine','draft' ];

    foreach( (array) $remove_views as $view )
    {
        if( isset( $views[$view] ) )
            unset( $views[$view] );
    }
    return $views;
} );


  ?>
