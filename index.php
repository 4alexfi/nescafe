<?php get_header(); ?>

      <div class="header__sock header-sock">
        <h1 class="header-sock__title"><?=pll__('say_main')?></h1>

        <div class="header-sock__middle">

          <form class="header-sock__form form js-form" method="POST" action="<?=home_url('/');?>" autocomplete="off">
            <ul class="form__list">
              <li class="form__error js-form_error">
                <span class="form__empty"><?= pll__('no_words_no_socks');?></span>
              </li>
              <li class="form__item">
                <textarea name="sock[content]" class="form__input form__input--textarea js-input" type="text" placeholder="<?=pll__('write_something')?>" rows="1"></textarea>
              </li>
              <li class="form__item">
                <input name="sock[name]" class="form__input js-input" type="text" placeholder="<?=pll__('your_name')?>" />
              </li>
              <li class="form__item">
                <?php
                $date = new DateTime();
                $timestamp = floor($date->getTimestamp() / $timestamp_check) * $timestamp_check;
                ?>
                <input name="timestamp" type="hidden" value="<?= $timestamp ?>"/>
                <input name="approve" type="hidden" />
                <input class="form__submit" type="submit" value="<?=pll__('create_sock')?>" />
              </li>
            </ul>
          </form>

        </div>

        <div class="header-sock__bottom"></div>
      </div>

      <div class="header__line header-line">
				<ul class="header-line__social social">
					<li class="social__item"><a onclick="Share.vkontakte('<?=addslashes($page_url)?>','<?=addslashes($page_title)?>','<?=addslashes($page_thumb_url)?>','<?=addslashes($page_description)?>')" class="social__link social__link--vk"></a></li>
					<li class="social__item"><a onclick="Share.facebook('<?=addslashes($page_url)?>','<?=addslashes($page_title)?>','<?=addslashes($page_thumb_url)?>','<?=addslashes($page_description)?>')" class="social__link social__link--fb"></a></li>
					<li class="social__item"><a onclick="Share.odnoklassniki('<?=addslashes($page_url)?>','<?=addslashes($page_title)?>','<?=addslashes($page_thumb_url)?>','<?=addslashes($page_description)?>')" class="social__link social__link--ok"></a></li>
				</ul>

        <a class="header-line__about header-line__link js-about_btn"><?= pll__('about_project');?></a>
      </div>
    </header>
    <main class="main">
      <div class="main__content js-main_content">

				<?php

				$args = array(
					'post_type' => 'post',
					'posts_per_page' => 5,
					'post_status' => 'publish',
          'category_name' => 'Featured'
				);

				$socks = new WP_Query( $args );

				while( $socks->have_posts() ) {
					$socks->the_post();
					?>
        <div class="main__sock sock">
          <div class="sock__active">
            <div class="sock__top"></div>
            <div class="sock__body">
              <p class="sock__text">
								<?php the_content() ?>
              </p>
            </div>
            <div class="sock__bottom"></div>
            <span class="sock__label"><?php the_title('', '') ?></span>
          </div>

          <div class="sock__copy">
            <div class="sock__top"></div>
            <div class="sock__body">
              <p class="sock__text">
								<?php the_content() ?>
              </p>
            </div>
            <div class="sock__bottom"></div>
          </div>
        </div>
					<?php
				}
				?>
      </div>

      <div class="main__more more">
        <a class="more__btn js-more_btn">
          <span class="more__text"><?=pll__('more_socks')?></span>
          <div class="more__spinner">
            <span class="more__spinner-image"></span>
          </div>
        </a>
      </div>
    </main>


<?php get_footer(); ?>
