<?php
global $timestamp_check;
$timestamp_check = 3;

if (isset($_POST['sock'])) {
	$sock = $_POST['sock'];

	$date = new DateTime();
	$curr_timestamp = $date->getTimestamp();

	$timestamp = $_POST['timestamp'];


	if ($timestamp % $timestamp_check == 0 && $curr_timestamp - $timestamp > $timestamp_check + 4 && empty($_POST['approve'])) {
		// Create post object
		$my_post = array();
		$name = uniqid();
		$my_post['post_title']    = $sock['name'];
		$my_post['post_name']     = $name;
		$my_post['post_content']  = $sock['content'];
		$my_post['post_status']   = 'publish';

		// Insert the post into the database
		$post_id = wp_insert_post( $my_post );

		wp_redirect( home_url('/'.$name) );

		exit;
	}
}

global $sock;
global $page_title;
global $page_description;
global $page_url;
global $page_thumb_url;

$socks_count = wp_count_posts();

$socks_count = $socks_count->publish + $socks_count->draft;

$sock = get_post();

$page_title = get_bloginfo('name');
$page_description = get_bloginfo('description');
$page_url = home_url('/');

$options_options = get_option( 'options_option_name' ); // Array of All Options
$country_counter = $options_options['country_counter'];
$page_thumb_url = $options_options['share_thumb'];

if (!is_home() && isset($sock)) {
	$page_title = $sock->post_title;
	$page_description = $sock->post_content;
	$page_url = home_url('/'.$sock->post_name);
}

?>
<!doctype html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<title><?php bloginfo('name'); ?></title>
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<meta name="viewport" content="width=1024">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,900&amp;subset=cyrillic-ext" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&amp;subset=cyrillic" rel="stylesheet">
	<!-- utilities.js must be on top -->

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KXPBRFH');</script>
<!-- End Google Tag Manager -->

<?php wp_head(); ?>

<meta property="og:url"           content="<?= $page_url ?>" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="<?= $page_title ?>" />
	<meta property="og:description"   content="<?= $page_description ?>" />
	<meta property="og:image"         content="<?= $page_thumb_url ?>" />

</head>
<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KXPBRFH"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
  <div class="body__wrapper">

    <header class="header">
      <ul class="header__stats stats">
        <li class="stats__item">

          <ul class="stats__counter counter js-counter" data-number="<?= $socks_count ?>">
            <li class="counter__item">
              <p class="counter__text"></p>
            </li>
            <li class="counter__item">
              <p class="counter__text"></p>
            </li>
            <li class="counter__item">
              <p class="counter__text"></p>
            </li>
            <li class="counter__item">
              <p class="counter__text"></p>
            </li>
            <li class="counter__item">
              <p class="counter__text"></p>
            </li>
            <li class="counter__item">
              <p class="counter__text"></p>
            </li>
          </ul>

          <p class="stats__text"><?= pll__('header_top_left')?></p>
        </li>
        <li class="stats__item">
          <ul class="stats__counter counter js-counter" data-number="<?=$country_counter?>">
            <li class="counter__item">
              <p class="counter__text"></p>
            </li>
            <li class="counter__item">
              <p class="counter__text"></p>
            </li>
            <li class="counter__item">
              <p class="counter__text"></p>
            </li>
            <li class="counter__item">
              <p class="counter__text"></p>
            </li>
            <li class="counter__item">
              <p class="counter__text"></p>
            </li>
            <li class="counter__item">
              <p class="counter__text"></p>
            </li>
          </ul>

          <p class="stats__text stats__text--right"><?= pll__('header_top_right')?></p>
        </li>
      </ul>
			<div class="header__santa">
        <img class="santa js-santa" src="<?=get_template_directory_uri()?>/img/artwork/img-hero__static.jpg" data-wink="<?=get_template_directory_uri()?>/img/artwork/img-hero__wink.jpg" data-gif="<?=get_template_directory_uri()?>/img/artwork/img-hero.gif" width="443" height="482" />
      </div>
